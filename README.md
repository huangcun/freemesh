# FreeMesh router default
Connect to the router at 192.168.1.1

# FreeMesh setup instructions

1. Connect the node (blue WAN port) to the router (yellow LAN port). Note - Both wifi lights should be alternating to indicate the node is in initialize mode.\
![Step 1 Image](images/Step1.PNG)
1. Once node’s wifi lights are flashing in sync, The node can be unplugged and moved.\
![Step 2 Image](images/Step2.PNG)
1. Once the node has been moved and powered on, You should see both wifi lights turn on solid. This means the node has a connection to the router. Note: It can take a few minutes for the wifi lights to come on after boot. If wifi lights do not come on after a few minutes, the node will probably need to be moved to a different location. 
1. Repeat steps 1 through steps 3 for each additional node.

Click the image below for the YouTube video

[![YouTube video](images/thumbnail.png)](http://www.youtube.com/watch?v=7tO_3ty7gps)

## To Compile

**Note: Before compiling install 'make' utility and development tools. You can find resources at https://openwrt.org/docs/guide-developer/build-system/install-buildsystem.**
1. Clone repo - git clone https://gitlab.com/slthomason/freemesh.git
1. cd /freemesh
1. Make a directory for the firmware - mkdir \<path to store builds\> 
1. Compile router - ./compile.sh zbt-we1326
1. Copy bin - cp bin/targets/ramips/mt7621/openwrt-ramips-mt7621-zbtlink_zbt-we1326-squashfs-sysupgrade.bin \<path to directory from step 3\>/we1326.router.bin
1. Compile node - ./compile.sh zbt-we826-16m
1. Copy bin - cp bin/targets/ramips/mt7620/openwrt-ramips-mt7620-zbtlink_zbt-we826-16m-squashfs-sysupgrade.bin \<path to directory from step 3\>/we826.node.bin

## Changing Settings
Currently you can change SSIDs and keys that will sync to the nodes. Changing IPs, channel, etc will require you to reset node and re-connect to the router. 
Feel free to extend this functionality at https://gitlab.com/slthomason/freemesh/blob/master/package/freemesh/cfg-router/files/freemesh/www/handler.cgi and submit a pull request.

# 4G LTE Configuration
**4G services not provided. You will still need to activate with your carrier.**  
Edit the /etc/config/network config Make sure the entry below is in the config.  

config interface wan  
\# option ifname ppp0 \# on some carriers enable this line  
option pincode 1234  
option device /dev/ttyUSB0  
option apn your.apn  
option service umts  
option proto 3g  

## T-Mobile
The /etc/config/network should look similar to this:  
config interface 'wan_ppp'  
option device '/dev/ttyUSB2'  
option metric '20'  
option apn 'fast.t-mobile.com'  
option service 'umts'  
option proto '3g'  

**Note: If your sim card doesn't have a pincode you can omit that in the config**

## Ethernet Backhaul
FreeMesh nodes can be connected to the router via an Ethernet cable creating a wired backhaul. Ethernet backhaul also works with managed switches that have STP support.  
Connect the node(s) to the router using a LAN to LAN connection. 
STP is enabled to prevent loops in network bridge.  
When Ethernet is disconnected the connection will failover to wireless.  
**Note: It could take up to 1 minute to connect to wireless after disconnecting from Ethernet.**  
![Backhaul Diagram](images/backhaul.png)

