#!/bin/sh
#run on startup each time

echo "First Boot";
uci set system.@system[0].hostname="fremesh";

/freemesh/flashled.sh 0 &

#setup cron job ....
crontab -l > /etc/crontabs/root;
echo "0-59/1 * * * * /freemesh/check_router_ap.sh" >> /etc/crontabs/root;
